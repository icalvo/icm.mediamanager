﻿Imports Icm.MediaManager.Core
Imports Icm.MediaManager.Data.Services
Imports Icm.MediaManager.Data

Public Class App

    Private Shared _repo As IFileRepository
    Private Shared _videoSrv As VideoService

    Public Shared Sub Main()
        Dim app As New App
        app.Start()
    End Sub

    Private Sub Start()
        Dim root = "C:\Users\Ignacio\Documents\etrayz\Video\Otros"

        Console.WriteLine("mediaman 1.0")
        Console.WriteLine("------------")
        Console.WriteLine("Initializating...")

        _videoSrv = New VideoService
        _repo = New EntityFileRepository

        Dim fso As New FileSystemObservable(root, _repo)
        fso.Check()
        Do
            Console.Write("MM {0}>", root)
            Dim command = Console.ReadLine()
            Select Case command
                Case "ls"
                    For Each item In _repo.All.Where(Function(vid) vid.StartsWith(root))
                        Console.WriteLine(item)
                    Next
                Case "rr"
                    Pruebas2()
                Case "cd"

                Case "exit", "quit"
                    Exit Do
            End Select
        Loop
    End Sub

    Private Sub Pruebas2()
        Dim root = "C:\Users\Ignacio\Documents\etrayz\Video\Otros"
        Dim mindur = TimeSpan.FromMinutes(7).TotalSeconds
        Dim maxdur = TimeSpan.FromMinutes(15).TotalSeconds

        Dim filter As New VideoCriteria({".mp4", ".mov", ".mpg", ".mpeg", ".wmv", ".avi"}, {}, {"[M]"}, 7, 15)

        filter.Extensions.Add(".mp4")
        filter.Extensions.Add(".mov")
        filter.Extensions.Add(".wmv")
        filter.Extensions.Add(".mpg")
        filter.Extensions.Add(".mpeg")
        filter.Extensions.Add(".mp4")
        filter.Extensions.Add(".avi")
        filter.Contains.Add("[M]")
        filter.MinDuration = TimeSpan.FromMinutes(7)
        filter.MaxDuration = TimeSpan.FromMinutes(15)
        For Each vid In _videoSrv.Search(filter)
            Console.WriteLine(vid.Span.Value.ToString("mm\:ss") & " " & vid.Name.Substring(root.Length + 1))
        Next
    End Sub

    Private Sub Pruebas()
        Dim root = "C:\Users\Ignacio\Videos"

        Dim di As New System.IO.DirectoryInfo(root)

        Dim ficheros = di.EnumerateFiles("*.*", IO.SearchOption.AllDirectories)

        Dim datos = From fi In ficheros
                    Select
                        Fichero = fi,
                        MediaInfo = New MediaInfo(fi.FullName)
                    Where
                        {".avi", ".mp4", ".wmv", ".mov", ".mkv", ".mpg", ".mpeg"}.Contains(Fichero.Extension) AndAlso
                        Not Fichero.Name.Contains("[M]") AndAlso
                        MediaInfo.Duration > TimeSpan.FromMinutes(3) AndAlso
                        MediaInfo.Duration < TimeSpan.FromMinutes(10)

        For Each dato In datos
            Console.WriteLine(dato.MediaInfo.Duration.Value.ToString("mm\:ss") & " " & dato.Fichero.FullName.Substring(root.Length + 1))
        Next
        Console.ReadLine()
    End Sub

End Class
