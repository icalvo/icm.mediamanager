﻿Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Imports Icm.MediaManager.Core

Public Class VideoViewModel
    Inherits ObservableObject

    Private _video As Video

    Property Video As Video
        Get
            Return _video
        End Get
        Private Set(value As Video)
            _video = value
        End Set
    End Property

    Public Sub New(vid As Video)
        Video = vid
    End Sub

    Property Name As String
        Get
            Return _video.Name
        End Get
        Set(value As String)
        End Set
    End Property
End Class
