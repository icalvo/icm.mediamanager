Imports System.Runtime.CompilerServices
Imports System.ComponentModel


Public MustInherit Class ObservableObject
    Implements INotifyPropertyChanged

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Protected Sub SetProperty(Of T)(ByRef field As T, value As T, <CallerMemberName> Optional propName As String = Nothing)
        If (Not EqualityComparer(Of T).Default.Equals(field, value)) Then
            field = value
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propName))
        End If
    End Sub

End Class
