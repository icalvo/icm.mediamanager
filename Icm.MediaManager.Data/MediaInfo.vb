﻿Public Class MediaInfo

    Private Shared ReadOnly _minfo As New MediaInfoDLL()

    Private _filename As String
    Private _duration As TimeSpan?

    Public Sub New(fn As String)
        _filename = fn
    End Sub


    ReadOnly Property Duration() As TimeSpan?
        Get
            If Not _duration.HasValue Then
                _minfo.Open(_filename)
                Dim resultDLL = _minfo.Get_(0, 0, "Duration")
                _minfo.Close()

                Dim result As Long
                If resultDLL = "" Then
                    _duration = Nothing
                Else
                    result = CLng(resultDLL)
                    _duration = TimeSpan.FromMilliseconds(result)
                End If
            End If
            Return _duration
        End Get
    End Property

End Class
