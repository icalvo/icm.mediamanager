﻿Imports System.Data.Entity
Imports System.Data.Entity.Infrastructure
Imports Icm.MediaManager.Core

''' <summary>
''' 
''' </summary>
''' <remarks>
''' The class is Friend because context cannot be directly used for accesing data.
''' Instead, the public classes of Icm.MediaManager.Services must be used.
''' </remarks>
Friend Class VideoContext
    Inherits DbContext

    Public Sub New()
        Debug.Write(Database.Connection.ConnectionString)
    End Sub

    Overridable Property Videos As IDbSet(Of Video)

End Class
