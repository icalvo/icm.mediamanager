﻿Namespace Services

    Public Interface IFileRepository

        Sub Add(fullName As String)
        Sub Remove(fullName As String)
        Sub Rename(fullName As String, newFullName As String)
        Function Has(fullName As String) As Boolean
        Function All() As IEnumerable(Of String)

    End Interface

End Namespace
