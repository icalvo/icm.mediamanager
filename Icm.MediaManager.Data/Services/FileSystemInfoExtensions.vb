﻿Imports System.Runtime.CompilerServices
Imports System.IO

Namespace Services

    Public Module FileSystemInfoExtensions

        <Extension>
        Public Function RelativeTo(fullPath As String, root As String) As String
            Return fullPath.Substring(root.Length)
        End Function

        <Extension>
        Public Function RelativeTo(fsi As FileSystemInfo, root As String) As String
            Return fsi.FullName.Substring(root.Length)
        End Function

    End Module

End Namespace
