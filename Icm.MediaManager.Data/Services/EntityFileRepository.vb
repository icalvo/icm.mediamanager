﻿Imports System.IO
Imports Icm.MediaManager.Core

Namespace Services

    Public Class EntityFileRepository
        Implements IFileRepository

        Private ReadOnly _store As VideoContext

        Public Sub New()
            _store = New VideoContext
        End Sub

        Friend Sub New(context As VideoContext)
            _store = context
        End Sub

        Public Sub FillMetadata(vid As Video)
            If Not vid.Duration.HasValue Then
                Dim minfo = New MediaInfo(vid.Name)

                If minfo.Duration.HasValue Then
                    vid.Duration = CLng(minfo.Duration.Value.TotalSeconds)
                Else
                    vid.Duration = Nothing
                End If
            End If
        End Sub

        Public Sub Add(fullName As String) Implements IFileRepository.Add
            Dim vid = _store.Videos.Create
            vid.Name = fullName
            FillMetadata(vid)
            _store.Videos.Add(vid)
            _store.SaveChanges()
        End Sub

        Public Function Has(fullName As String) As Boolean Implements IFileRepository.Has
            Return _store.Videos.Any(Function(vid) vid.Name = fullName)
        End Function

        Public Sub Remove(fullName As String) Implements IFileRepository.Remove
            Dim vid = _store.Videos.Single(Function(vid2) vid2.Name = fullName)
            _store.Videos.Remove(vid)
            _store.SaveChanges()
        End Sub

        Public Sub Rename(fullName As String, newFullName As String) Implements IFileRepository.Rename
            Dim vid = _store.Videos.Single(Function(vid2) vid2.Name = fullName)
            vid.Name = newFullName
            _store.SaveChanges()
        End Sub

        Public Function All() As IEnumerable(Of String) Implements IFileRepository.All
            Return _store.Videos.Select(Function(vid) vid.Name)
        End Function
    End Class

End Namespace
