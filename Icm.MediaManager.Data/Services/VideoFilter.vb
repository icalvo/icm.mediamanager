Imports Icm.MediaManager.Core

Namespace Services

    Public Class VideoCriteria

        Private ReadOnly _extensions As New List(Of String)
        Private ReadOnly _contains As New List(Of String)
        Private ReadOnly _notContains As New List(Of String)

        Public Sub New(extensions As IEnumerable(Of String),
                       contains As IEnumerable(Of String),
                       notContains As IEnumerable(Of String),
                       minDur As TimeSpan?,
                       maxDur As TimeSpan?)

            _extensions.AddRange(extensions)
            _contains.AddRange(contains)
            _notContains.AddRange(notContains)
            MinDuration = minDur
            MaxDuration = maxDur
        End Sub

        Public Sub New(extensions As IEnumerable(Of String),
                       contains As IEnumerable(Of String),
                       notContains As IEnumerable(Of String),
                       minDur As Long?,
                       maxDur As Long?)

            _extensions.AddRange(extensions)
            _contains.AddRange(contains)
            _notContains.AddRange(notContains)
            MinDuration = TimeSpan.FromMinutes(CDbl(minDur))
            MaxDuration = TimeSpan.FromMinutes(CDbl(maxDur))
        End Sub


        ReadOnly Property Extensions As ICollection(Of String)
            Get
                Return _extensions
            End Get
        End Property

        ReadOnly Property Contains As ICollection(Of String)
            Get
                Return _contains
            End Get
        End Property

        ReadOnly Property NotContains As ICollection(Of String)
            Get
                Return _notContains
            End Get
        End Property

        Property MinDuration As TimeSpan?
        Property MaxDuration As TimeSpan?

    End Class

End Namespace
