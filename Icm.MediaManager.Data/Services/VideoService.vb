﻿Imports Icm.MediaManager.Core
Imports LinqKit

Namespace Services

    Public Class VideoService

        Private ReadOnly _ctx As VideoContext

        Public Sub New()
            _ctx = New VideoContext
        End Sub

        Public Function Search(filter As VideoCriteria) As IEnumerable(Of Video)
            Dim predicate = PredicateBuilder.False(Of Video)()

            For Each ext In filter.Extensions
                predicate = predicate.Or(Function(vid) vid.Name.EndsWith(ext))
            Next
            For Each cont In filter.Contains
                predicate = predicate.Or(Function(vid) vid.Name.EndsWith(cont))
            Next
            If filter.MinDuration.HasValue Then
                Dim minSec = filter.MinDuration.Value.TotalSeconds
                predicate = predicate.And(Function(vid) vid.Duration.Value > minSec)
            End If
            If filter.MaxDuration.HasValue Then
                Dim maxSec = filter.MaxDuration.Value.TotalSeconds
                predicate = predicate.And(Function(vid) vid.Duration.Value < maxSec)
            End If
            Dim result = _ctx.Videos.AsExpandable.Where(predicate).ToList
            Debug.Print(result.ToString)
            Return result
        End Function


    End Class

End Namespace
