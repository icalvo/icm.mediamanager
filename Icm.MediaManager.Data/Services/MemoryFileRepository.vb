﻿Namespace Services

    Public Class MemoryFileRepository
        Implements IFileRepository

        Private store_ As New HashSet(Of String)
        Private _root As String

        ReadOnly Property Root As String
            Get
                Return _root
            End Get
        End Property

        Public Sub Add(fullName As String) Implements IFileRepository.Add
            store_.Add(fullName)
        End Sub

        Public Function All() As IEnumerable(Of String) Implements IFileRepository.All
            Return store_
        End Function

        Public Function Has(fullName As String) As Boolean Implements IFileRepository.Has
            Return store_.Contains(fullName)
        End Function

        Public Sub Remove(fullName As String) Implements IFileRepository.Remove
            store_.Remove(fullName)
        End Sub

        Public Sub Rename(fullName As String, newFullName As String) Implements IFileRepository.Rename
            Remove(fullName)
            Add(newFullName)
        End Sub

    End Class

End Namespace
