﻿Imports System.IO

Namespace Services

    Public Class FileSystemObservable

        Private ReadOnly _root As String
        Private WithEvents _fsw As System.IO.FileSystemWatcher
        Private ReadOnly _repo As IFileRepository

        Public Sub New(root As String, repo As IFileRepository)
            _root = root
            _repo = repo
            _fsw = New FileSystemWatcher(root)
            _fsw.IncludeSubdirectories = True
            _fsw.EnableRaisingEvents = True
        End Sub

        Public Sub Check()
            Console.WriteLine("Checking consistency...")
            Dim di As New DirectoryInfo(_root)

            Dim ficheros = di.EnumerateFiles("*.*", SearchOption.AllDirectories)

            Dim added, removed As Integer
            For Each fichero In ficheros.Where(Function(fichero2) Not _repo.Has(fichero2.FullName))
                _repo.Add(fichero.FullName)
                added += 1
            Next
            Console.WriteLine("{0} new files added to database", added)
            For Each dbfichero In _repo.All.Where(Function(dbfichero2) Not File.Exists(dbfichero2)).ToList
                _repo.Remove(dbfichero)
                removed += 1
            Next
            Console.WriteLine("{0} files removed from database", removed)

            Console.WriteLine("Done!")
        End Sub

        Private Sub _fsw_Created(sender As Object, e As FileSystemEventArgs) Handles _fsw.Created
            _repo.Add(e.FullPath)
        End Sub

        Private Sub _fsw_Deleted(sender As Object, e As FileSystemEventArgs) Handles _fsw.Deleted
            _repo.Remove(e.FullPath)
        End Sub

        Private Sub _fsw_Renamed(sender As Object, e As RenamedEventArgs) Handles _fsw.Renamed
            _repo.Rename(e.OldFullPath, e.FullPath)
        End Sub

    End Class

End Namespace
