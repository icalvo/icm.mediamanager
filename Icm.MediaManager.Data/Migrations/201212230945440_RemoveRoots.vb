Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class RemoveRoots
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.Videos", "RootId", "dbo.Roots")
            DropIndex("dbo.Videos", New String() { "RootId" })
            DropColumn("dbo.Videos", "RootId")
            DropTable("dbo.Roots")
        End Sub
        
        Public Overrides Sub Down()
            CreateTable(
                "dbo.Roots",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .Name = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            AddColumn("dbo.Videos", "RootId", Function(c) c.Int(nullable := False))
            CreateIndex("dbo.Videos", "RootId")
            AddForeignKey("dbo.Videos", "RootId", "dbo.Roots", "ID", cascadeDelete := True)
        End Sub
    End Class
End Namespace
