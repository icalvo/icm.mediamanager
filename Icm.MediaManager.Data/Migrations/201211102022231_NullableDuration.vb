Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class NullableDuration
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AlterColumn("dbo.Videos", "Duration", Function(c) c.Long())
        End Sub
        
        Public Overrides Sub Down()
            AlterColumn("dbo.Videos", "Duration", Function(c) c.Long(nullable := False))
        End Sub
    End Class
End Namespace
