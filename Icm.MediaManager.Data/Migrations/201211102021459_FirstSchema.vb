Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class FirstSchema
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Roots",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .Name = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            CreateTable(
                "dbo.Videos",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .Name = c.String(),
                        .Duration = c.Long(nullable := False),
                        .RootId = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.Roots", Function(t) t.RootId, cascadeDelete := True) _
                .Index(Function(t) t.RootId)
            
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.Videos", New String() { "RootId" })
            DropForeignKey("dbo.Videos", "RootId", "dbo.Roots")
            DropTable("dbo.Videos")
            DropTable("dbo.Roots")
        End Sub
    End Class
End Namespace
