﻿Public Class Video

    Overridable Property ID As Integer
    Overridable Property Name As String

    Overridable Property Duration As Long?

    Function Span() As TimeSpan?
        If Duration.HasValue Then
            Return TimeSpan.FromSeconds(Duration.Value)
        Else
            Return Nothing
        End If
    End Function
End Class
